# data_scientist_assignment

This project is broken down into the following sections:
1. Initialise - Importing packages, defining filepaths, defining helper functions, importing data files.
2. Exploratory data analysis - To understand the underlying data.
3. Assignment body - Answering questions 1 to 4.

